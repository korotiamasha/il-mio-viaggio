import Swiper from 'swiper/swiper-bundle';
import AirDatepicker from 'air-datepicker';
import localeEn from 'air-datepicker/locale/en';
import bootstrapSlider from 'bootstrap-slider/dist/bootstrap-slider';


(function ($) {
    /*accordion*/
    $('.faq__item-header').on('click', function () {
        let item = $(this).parent();
        if (item.hasClass('active')) {
            item.removeClass('active');
        } else {
            $('.faq__item.active').removeClass('active');
            item.addClass('active');
        }
    })

    /* news-slider*/
    const swiper = new Swiper('.news-slider', {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        navigation: {
            nextEl: '.news-slider-next',
            prevEl: '.news-slider-prev',
        },
        breakpoints: {
            575: {
                slidesPerView: 2,
            },

            992: {
                slidesPerView: 3,
            }
        }
    })

    /*datepicker*/
    new AirDatepicker('.datepicker', {
        locale: localeEn,
    })

    new AirDatepicker('.aside-datepicker', {
        locale: localeEn,
        inline: true,
    })

    /*select*/
    //select-local
    $('.select-local').select2({
        minimumResultsForSearch: -1,
        containerCssClass: "custom-select custom-select-local"
    });

    //select-count
    $('.select-count').select2({
        minimumResultsForSearch: -1,
        containerCssClass: "custom-select custom-select-count"
    });

    $('.select2-sort').select2({
        minimumResultsForSearch: -1,
        containerCssClass: "select-sort",
    });

    $('.select2-lang').select2({
        minimumResultsForSearch: -1,
        dropdownCssClass: "select-lang",
    });

    /*chose-date*/
    $('.js-choose-date').on('click', function () {
        $('.aside-date').addClass('active');
    });
    $('.aside-date__header').on('click', function () {
        $('.aside-date').removeClass('active');
    })

    /*range Slider*/
    if ($('.custom-range-slider').length > 0) {
        var sliderTime = new Slider("#range-time", {
            tooltip: 'always',
            min: 0,
            max: 30,
            value: [5, 20],
            step: 1,
            formatter: function (value) {
                return value + ' h.';
            }
        });
        var sliderPrice = new Slider("#range-price", {
            tooltip: 'always',
            min: 0,
            max: 1000,
            value: [89, 859],
            step: 1,
            formatter: function (value) {
                return '€ ' + value;
            }
        });
        var sliderDuration = new Slider("#range-duration", {
            tooltip: 'always',
            min: 1,
            max: 200,
            value: [40, 150],
            step: 1,
            formatter: function (value) {
                return value + ' hrs.';
            }
        });
    }

    /*filter*/
    $('.js-filter').on('click', function () {
        $('.filter').addClass('active');
        $('body').addClass('modal-open');
    });

    $('.filter__header, .js-cancel-filter').on('click', function () {
        $('.filter').removeClass('active');
        $('body').removeClass('modal-open');
    });

    /*modal*/
    $('.js-modal').on('click', function () {
        let id = $(this).attr('data-modal-id');
        $('#' + id).addClass('active');
        $('body').addClass('modal-open');
    });

    $(document).mouseup(function (e) {
        var div = $(".il-modal__content");
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            $('.il-modal').removeClass('active');
            $('body').removeClass('modal-open');
        }
    });

    /*header menu*/
    $('.header__burger').on('click', function () {
        $('.header__nav').toggleClass('active');
    });

})(jQuery)


